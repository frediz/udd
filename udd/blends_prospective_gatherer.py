#!/usr/bin/env python

"""
This script imports data about not yet uploaded packages prepared by Blends teams.
"""

from aux import parse_email
from gatherer import gatherer
from sys import stderr, exit
from os import listdir
from os.path import exists
from fnmatch import fnmatch
from psycopg2 import IntegrityError, InternalError, ProgrammingError, DataError
import re
import logging
import logging.handlers
from subprocess import Popen, PIPE
from debian import deb822
import email.Utils
from glob import glob

from upstream_reader import upstream_reader, edam_reader, bibtex_check
from datetime import datetime

debug=0

MAX_WNPP=100000

def get_gatherer(connection, config, source):
  return blends_prospective_gatherer(connection, config, source)

def RowDictionaries(cursor):
    """Return a list of dictionaries which specify the values by their column names"""

    description = cursor.description
    if not description:
        # even if there are no data sets to return the description should contain the table structure.  If not something went
        # wrong and we return NULL as to represent a problem
        return NULL
    if cursor.rowcount <= 0:
        # if there are no rows in the cursor we return an empty list
        return []

    data = cursor.fetchall()
    result = []
                                                                                          
    for row in data:
        resultrow = {}
        i = 0
        for dd in description:
            resultrow[dd[0]] = row[i]
            i += 1
        result.append(resultrow)
    return result


class blends_prospective_gatherer(gatherer):
  """
  Not yet uploaded packages prepared by Blends teams in Vcs
  """

  def __init__(self, connection, config, source):
    gatherer.__init__(self, connection, config, source)
    self.assert_my_config('table')

    self.log = logging.getLogger(self.__class__.__name__)
    if debug==1:
        self.log.setLevel(logging.DEBUG)
    else:
        self.log.setLevel(logging.INFO)
    handler = logging.handlers.RotatingFileHandler(filename=self.__class__.__name__+'.log',mode='w')
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - (%(lineno)d): %(message)s")
    handler.setFormatter(formatter)
    self.log.addHandler(handler)

    self.prospective = []

  def run(self):
    my_config = self.my_config
    cur = self.cursor()

    # find_itp_re = re.compile('\s+\*\s+.*(initial|ITP).+closes:\s+(Bug|)#(\d+)', flags=re.IGNORECASE|re.MULTILINE)
    # just check for the term "initial|ITP"
    find_itp_re = re.compile('(initial|ITP)', flags=re.IGNORECASE|re.MULTILINE)
    # might need enhancement (see http://www.debian.org/doc/manuals/developers-reference/pkgs.html#upload-bugfix)
    # --> /closes:\s*(?:bug)?\#\s*\d+(?:,\s*(?:bug)?\#\s*\d+)*/ig
    parse_itp_re = re.compile('^([A-Z]+): *([^\s]+) *-- *(.+)$')
    vcs_type_re = re.compile('Vcs-(Svn|Git|Bzr|Darcs|Hg|Cvs|Arch|Mtn)')
    
    cur.execute('TRUNCATE %s' % my_config['table'])
    cur.execute('TRUNCATE upstream_metadata')
    cur.execute('TRUNCATE bibref')
    cur.execute('TRUNCATE edam')
    cur.execute('TRUNCATE registry')
    cur.execute("PREPARE check_source (text) AS SELECT COUNT(*) FROM sources WHERE source = $1")
    cur.execute("PREPARE check_new_source (text) AS SELECT COUNT(*) FROM new_sources WHERE source = $1")
    cur.execute("PREPARE check_reference (text) AS SELECT COUNT(*) FROM bibref WHERE source = $1")

    cur.execute("""PREPARE check_itp (int) AS
                  SELECT id, arrival, submitter, owner, title, last_modified, submitter_name, submitter_email, owner_name, owner_email
                    FROM bugs
                    WHERE id = $1 AND package = 'wnpp' and source = 'wnpp' """)

    u_dirs = listdir(my_config['path'])

    pkgs = []
    bibrefs = []
    bibrefs_keys = {}
    umetadata = []
    umetadata_keys = {}
    edamdata = []
    registrydata = []

    for u in sorted(u_dirs):
      if u.startswith('.'): # it might happen that the machine readable gatherer did not read files correctly
        self.log.error("Invalid file or directory in %s: %s" % (my_config['path'], u))
        continue
      upath=my_config['path']+'/'+u
      sources = []
      for file in listdir(upath):
        if fnmatch(file, '*.changelog'):
          sources.append(re.sub("\.changelog", "", file))
      for sourcevcs in sorted(sources):
        sprosp = {}
        # Read Vcs fields
        vcsfile = upath+'/'+sourcevcs+'.vcs'
        try:
          vcs = open(vcsfile,'r')
        except:
          self.log.warning("Unable to open Vcs file for source '%s' (%s)" % (sourcevcs, vcsfile))
        for line in vcs.readlines():
          (field,value) = line.split(': ')
          field = field.strip()
          value = value.strip()
          if field == 'Blend':
            sprosp['blend'] = value
          elif field == 'Vcs-Browser':
            sprosp['vcs_browser'] = value
          else:
            matchvcs = vcs_type_re.match(field)
            if matchvcs:
              sprosp['vcs_type'] = matchvcs.groups()[0]
              sprosp['vcs_url'] = value
        vcs.close()

        # Read output of dpkg-parsechangelog
        if exists('/usr/bin/dpkg-parsechangelog'):
          p = Popen("LC_ALL=C dpkg-parsechangelog -l"+upath+'/'+sourcevcs+'.changelog', shell=True, bufsize=4096,
                    stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True)
          errstring = p.stderr.read()
          p.wait()
          if p.returncode > 0 or errstring != '':
            self.log.warning("Error parsing changelog of '%s' of %s\n %s:" % (sourcevcs, sprosp['blend'], errstring))
        else:
            self.log.error("dpkg-parsechangelog file does not exist - make sure dpkg-dev is installed")
            exit(1)
        source = sourcevcs
        prospectiveset = False
        for stanza in deb822.Sources.iter_paragraphs(p.stdout):
          if sourcevcs != stanza['source']:
            self.log.warning("Something is wrong with changelog data of package '%s' of %s.  Changelog says source = '%s'." % (sourcevcs, sprosp['blend'], stanza['source']))
            source = stanza['source']
          if not prospectiveset:
            cur.execute("EXECUTE check_source (%s)", (source,))
            prospective = True
            if cur.fetchone()[0] > 0:
              prospective = False

            cur.execute("EXECUTE check_new_source (%s)", (source,))
            if cur.fetchone()[0] > 0:
              prospective = False
            prospectiveset = True

          for prop in ('source', 'distribution'):
            if stanza.has_key(prop):
              sprosp[prop] = stanza[prop]
            else:
             self.log.warning("Missing property %s in changelog of '%s' of %s" % (prop, source, sprosp['blend']))
          sprosp['chlog_version']    = stanza['version']
          if stanza.has_key('maintainer'):
            sprosp['changed_by']       = stanza['maintainer']
            (name, email) = parse_email(stanza['maintainer'])
            sprosp['changed_by_name']  = name
            sprosp['changed_by_email'] = email
          else:
            sprosp['changed_by']       = ''
            sprosp['changed_by_name']  = ''
            sprosp['changed_by_email'] = ''
            self.log.warning("Can not obtain maintainer e-mail from changelog of '%s' of %s" % (source, sprosp['blend']))
          if stanza.has_key('date'):
            sprosp['chlog_date']       = stanza['date']
          else:
            sprosp['chlog_date']       = ''
            self.log.warning("Can not obtain changed data from changelog of '%s' of %s" % (source, sprosp['blend']))
          sprosp['closes'] = []
          if stanza.has_key('closes'):
            for bug in stanza['closes'].split(' '):
              sprosp['closes'].append(int(bug))
          changes                      = stanza['changes']
          sprosp['wnpp'] = 0
          sprosp['wnpp_type'] = ''
          sprosp['wnpp_desc'] = ''
#          if match: # try to make sure we are really dealing with ITPs
          if prospective:
            for iwnpp in sprosp['closes']:
              if iwnpp == 12345: # that seems to be a fake ITP
                self.log.debug("Fake WNPP no. 12345 in changelog of '%s' of %s" % (source, sprosp['blend']))
                continue
              elif iwnpp > MAX_WNPP: # this also seems to be a fake ITP
                self.log.debug("Fake WNPP no. > MAX_WNPP (%i) in changelog of '%s' of %s" % (iwnpp, source, sprosp['blend']))
                continue
              elif iwnpp > 0:
                sprosp['wnpp'] = iwnpp
                try:
                  cur.execute("EXECUTE check_itp (%s)", (iwnpp,))
                except DataError, err:
                  self.log.error("DataError when trying to check WNPP number %s of '%s' of %s (%s)" % (str(iwnpp), source, sprosp['blend'], str(err)))
                  break
                if cur.rowcount > 0:
                  wnppbug = RowDictionaries(cur)[0]
                  itpmatch = parse_itp_re.search(wnppbug['title'])
                  if itpmatch:
                    sprosp['wnpp_type'] = itpmatch.groups()[0]
                    sprosp['wnpp_desc'] = itpmatch.groups()[2]
                    if source != itpmatch.groups()[1]:
                      self.log.info("Source name of '%s' of %s differs from name in %s bug: package name = %s, short description = %s" % (source, sprosp['blend'], itpmatch.groups()[0], itpmatch.groups()[1], itpmatch.groups()[2]))
                  else:
                    self.log.warning("Cannot parse ITP bug %i for package '%s' of %s: `%s`" % (iwnpp, source, sprosp['blend'], wnppbug['title']))
                else:
                  self.log.debug("ITP bug %i for package '%s' of %s is not open any more or can otherwise not be found" % (iwnpp, source, sprosp['blend']))
                  continue # Try other bug number if exists
                break
            match = find_itp_re.search(changes)
            if not match and sprosp['wnpp_type'] == '' and len(sprosp['closes']) > 0 and sprosp['wnpp'] > 0:
              sprosp['wnpp'] = 0
              self.log.warning("Bug %s closed in changelog of package '%s' of %s does not seem to be an ITP" % (str(sprosp['closes']), source, sprosp['blend']))
#        if source not in sprosp:
#          sprosp['source'] = source
#          self.log.warning("Set at least source name '%s' after reading changelog failed in Blend %s" % (sprosp['source'], sprosp['blend']))

        # Read Copyright file if specifying Format in the first line
        cprfile = upath+'/'+sourcevcs+'.copyright'
        try:
          cpr = open(cprfile,'r')
        except:
          self.log.debug("Unable to open Copyright file for source '%s' of %s (%s)" % (source, sprosp['blend'], cprfile))
          cpr = None
        linenr = 0
        found_files = False
        sprosp['license'] = ''
        if cpr:
          for line in cpr.readlines():
            line = line.strip()
            if line == '':
              if found_files:
                found_files = False
                break # We might leave the 'Files: *' paragraph again
              continue
            try:
              (field,value) = line.split(': ')
            except ValueError:
              # either no DEP5 file or no line we want to read here
              continue
            if linenr == 0:
              if field != 'Format':
                self.log.debug("Copyright file for source '%s' of %s does not seem to regard DEP5.  Found line `%s`" % (source, sprosp['blend'], line.strip()))
                found_files = True # one flag is enough to control this - we do not need another warning in the logs
                break
            linenr += 1
            field = field.strip()
            value = value.strip()
            if field == 'Files' and value == '*':
              found_files = True
            if field == 'License' and found_files:
              sprosp['license'] = value
              break
          if not found_files:
            self.log.debug("No 'Files: *' specification found in copyright file for source '%s' of %s" % (source, sprosp['blend']))

    	# Try to read debian/control
    	ctrl = None
    	ctrlfile = upath+'/'+sourcevcs+'.control'
    	try:
    	  ctrl = open(ctrlfile,'r')
    	except:
    	  self.log.warning("Unable to open control file for source '%s' of %s (%s)" % (source, sprosp['blend'], ctrlfile))
    	if ctrl:
	  ictrl = deb822.Deb822.iter_paragraphs(ctrl)
	  src = ictrl.next()
	  # print 'SOURCE:', src      # print Source stanza
          if src.has_key('source'):
            if source != src['source']:
              self.log.error("Something is wrong with control data of package '%s' of %s.  Changelog says source = '%s'." % (source, sprosp['blend'], src['Source']))
          else:
    	    self.log.warning("Control file for source '%s' of %s is lacking source field" % (source, sprosp['blend']))
    	  if src.has_key('vcs-browser'):
    	    if sprosp['vcs_browser'] != src['vcs-browser']:
    	      tmp_prosp = re.sub('/$', '', sprosp['vcs_browser']) # ignore forgotten '/' at end of Vcs-Browser
              tmp_src = re.sub('/$', '', src['vcs-browser'])     # same with string in debian/control to enable comparison after further changes below
              tmp_src = re.sub('\.git;a=summary$', '.git', tmp_src)
              tmp_src = re.sub('/viewsvn/', '/wsvn/', tmp_src) # machine-readable gatherer implies /wsvn/ but specifying /viewsvn/ does no harm
              tmp_src = re.sub('/anonscm.debian.org/gitweb/\?p=', '/anonscm.debian.org/cgit/', tmp_src)
              tmp_src = re.sub('/anonscm.debian.org/git/([^?])', '/anonscm.debian.org/cgit/\\1', tmp_src) # Add missing '?p='
              tmp_src = re.sub('/\?op=log', '', tmp_src) # Some SVN URLs specify this parameter which should not be regarded here
    	      if tmp_prosp != tmp_src:
    	        tmp_src = re.sub('^git:', 'http:', tmp_src) # check for usual error in specifying Vcs-Browser by just leaving 'git:' as protocol
    	        if tmp_src == sprosp['vcs_browser']:
    	          self.log.error("%s of %s - Wrong Vcs-Browser: Use 'http:' instead of 'git:' in '%s' ('%s')." % (source, sprosp['blend'], src['Vcs-Browser'], sprosp['vcs_browser']))
    	        else:
                  tmp_prosp = re.sub('/trunk/?$', '', tmp_prosp) # sometimes the trailing trunk/ is forgotten which is no real problem
                  tmp_src   = re.sub('/trunk/?$', '', tmp_src)   # also in tmp_src there is sometimes a remaining /trunk
                  tmp_prosp = re.sub('^https*:', '', tmp_prosp) # accept http and https in both URLs
                  tmp_src   = re.sub('^https*:', '', tmp_src)
    	          if tmp_prosp != tmp_src:
                    if not  sprosp['vcs_browser'].startswith('git@salsa.debian.org:'): # do not warn about packages migrated to Salsa
                      self.log.warning("%s of %s - Differing Vcs-Browser:  Obtained from Vcs-Browser='%s' <-> control has '%s'." % (source, sprosp['blend'], sprosp['vcs_browser'], src['Vcs-Browser']))
          else:
    	    self.log.debug("Control file for source '%s' of %s is lacking Vcs-Browser field" % (source, sprosp['blend']))

    	  if src.has_key('Maintainer'):
            sprosp['maintainer']       = src['maintainer']
            (name, email) = parse_email(src['maintainer'])
            sprosp['maintainer_name']  = name
            sprosp['maintainer_email'] = email
          else:
    	    self.log.info("Control file for source '%s' of %s is lacking Maintainer field" % (source, sprosp['blend']))
            # set some reasonable non-NULL default since otherwise the import will be broken
            sprosp['maintainer_name']  = '??? - ' + sprosp['blend'] + ' packaging team ???'
            sprosp['maintainer_email'] = '<unknown@maintain.er>'
            sprosp['maintainer']       = sprosp['maintainer_name'] + ' ' + sprosp['maintainer_email']

          # make sure we get a component field in any case even if section field is missing
          sprosp['component'] = ''
          for prop in ('homepage', 'priority', 'section', 'uploaders', ):
            if src.has_key(prop):
              if prop == 'section':
                if src['section'].startswith('non-free'):
                  sprosp['component'] = 'non-free'
                  (dummy,sprosp['section']) = src['section'].split('/')
                  try:
                    if sprosp['license'] == '':
                      sprosp['license'] = 'non-free'
                  except:
                      sprosp['license'] = 'non-free'
                elif src['section'].startswith('contrib'):
                  sprosp['component'] = 'contrib'
                  (dummy,sprosp['section']) = src['section'].split('/')
                  if sprosp['license'] == '':
                    sprosp['license'] = 'free'
                else:
                  sprosp['component'] = 'main'
                  sprosp['section']   = src['section']
                  if sprosp['license'] == '':
                    sprosp['license'] = 'free'
              else:
                sprosp[prop] = src[prop]
            else:
              sprosp[prop] = ''
              if prop != 'uploaders':
                self.log.warning("Control file for source '%s' of %s is lacking %s field" % (source, sprosp['blend'], prop))
              else:
                self.log.debug("Control file for source '%s' of %s is lacking %s field" % (source, sprosp['blend'], prop))

          if sprosp['component'] == '':
            self.log.warning("Control file for source '%s' of %s is lacking section field -> force component to 'main'" % (source, sprosp['blend']))
            # Component field should either be main, contrib or non-free - so we assume main if we do not know better
            sprosp['component'] = 'main'

          if prospective:
            pkg = ictrl.next()
            valid_pkg_info = True
            while pkg:
              pprosp = {}
              for sprop in sprosp.keys():
                pprosp[sprop] = sprosp[sprop]

              if pkg.has_key('package'):
                pprosp['package'] = pkg['package']
              else:
                self.log.error("Control file for source '%s' of %s is lacking Package field" % (source, sprosp['blend']))
                valid_pkg_info = False
              if pkg.has_key('description'):
                if len(pkg['description'].split("\n",1)) > 1:
                  pprosp['long_description'] = pkg['description'].split("\n",1)[1]
                else:
                  pprosp['long_description'] = ''
                pprosp['description'] = pkg['description'].split("\n",1)[0].strip()
              else:
                if pprosp.has_key('package'):
                  self.log.error("Control file for source '%s' of %s has no desription for Package %s" % (source, sprosp['blend'], pprosp['package']))
                  valid_pkg_info = False
                else:
                  # self.log.error("Control file for source '%s' of %s seems to miss package information" % (source, sprosp['blend']))
                  self.log.info("Control file for source '%s' of %s seems to contain some comments which can not be parsed/ignored with this python-debian version" % (source, sprosp['blend']))
                  valid_pkg_info = False
              if valid_pkg_info:
                pkgs.append(pprosp)
              try:
                pkg = ictrl.next()
                valid_pkg_info = True
              except:
                break

        upstream = None
        ufile = upath+'/'+sourcevcs+'.upstream'
    	if exists(ufile):
          upstream = upstream_reader(ufile, source, self.log, sprosp['blend'])
          for umeta in upstream.get_umetadata():
            key = "%(source)s_%(key)s" % umeta
            if not umetadata_keys.has_key(key):
              umetadata_keys[key] = sourcevcs
              umetadata.append(umeta)
            else:
              self.log.error("Upstream metadata key '%s' of package %s of Blend %s in Vcs dir %s was seen in Vcs %s before." % (key, source, sprosp['blend'], sourcevcs, umetadata_keys[key]))
          if upstream.references:
            upstream.parse()
            for ref in upstream.get_bibrefs():
              key = "%(source)s_%(key)s_%(package)s_%(rank)s" % ref
              if not bibrefs_keys.has_key(key):
                bibrefs_keys[key] = sourcevcs
                bibrefs.append(ref)
              else:
                self.log.error("Bibref key '%s' of package %s of Blend %s in Vcs dir %s was seen in Vcs %s before." % (key, source, sprosp['blend'], sourcevcs, bibrefs_keys[key]))
          else:
            if upstream.registry:
              upstream.setregistry(upstream.registry)
          if upstream.registry:
            if isinstance(upstream.registry, dict):
              registrydata.append(upstream.registry)
            else:
              for reg in upstream.registry:
                registrydata.append(reg)
              self.log.debug("Registry data for source '%s' of %s: %s" % (source, sprosp['blend'], str(registrydata)))
          else:
              self.log.debug("No registry data for source '%s' of %s: upstream.registry = %s" % (source, sprosp['blend'], str(upstream.registry)))          
        edam = None
        efile = upath+'/'+sourcevcs+'.edam'
        if exists(efile):
          edam = edam_reader(efile, source, self.log, sprosp['blend'])
        else:
          edamfiles = glob(upath+'/'+sourcevcs+'*edam')
          for efile in edamfiles:
            if exists(efile):
              edam = edam_reader(efile, source, self.log, sprosp['blend'])
        if edam != None:
          ed = edam.get_edam()
          if ed:
            edamdata.append(ed)

    cur.execute("""PREPARE package_insert AS INSERT INTO %s
        (blend, package, source,
         maintainer, maintainer_name, maintainer_email,
         changed_by, changed_by_name, changed_by_email,
         uploaders,
         description, long_description,
         homepage, component, section, priority,
         vcs_type, vcs_url, vcs_browser,
         wnpp, wnpp_type, wnpp_desc,
         license, chlog_date, chlog_version)
        VALUES
        ( $1, $2, $3,
          $4, $5, $6,
          $7, $8, $9,
          $10,
          $11, $12,
          $13, $14, $15, $16,
          $17, $18, $19,
          $20, $21, $22,
          $23, $24, $25)
        """ %  (my_config['table']))
    pkgquery = """EXECUTE package_insert
      (%(blend)s, %(package)s, %(source)s,
       %(maintainer)s, %(maintainer_name)s, %(maintainer_email)s,
       %(changed_by)s, %(changed_by_name)s, %(changed_by_email)s,
       %(uploaders)s,
       %(description)s, %(long_description)s,
       %(homepage)s, %(component)s, %(section)s, %(priority)s,
       %(vcs_type)s, %(vcs_url)s, %(vcs_browser)s,
       %(wnpp)s, %(wnpp_type)s, %(wnpp_desc)s,
       %(license)s, %(chlog_date)s, %(chlog_version)s)"""


    pkgs_list = []  # List containing packages which violate Primary Key Condition
    dup_pkgs = []
    for p in pkgs:
        if p['package'] in pkgs_list:
            self.log.error("Duplicated package name '%s' in source %s in Blend %s" % (p['package'], p['source'], sprosp['blend']))
            dup_pkgs.append(p)
            continue
        if not 'source' in p:
            self.log.error("FIXME: Source missing for binary package '%s' in Blend %s" % (p['package'], sprosp['blend']))
            continue
        pkgs_list.append(p['package'])
        try:
            cur.execute(pkgquery, p)
            #cur.executemany(pkgquery, pkgs)
        except ProgrammingError as err:
            print("ProgrammingError: Error while inserting packages (%s)" % str(err))
            raise
        except KeyError as err:
            print("KeyError: Error while inserting packages (%s)" % str(err))
            raise
        except IntegrityError as err:
            print "Duplicate Key Error while inserting packages - this should not happen for %s" % p.package, err
            raise
        else:
            self.connection.commit()

    # Once all the prospective packages are inserted into UDD, make sure that 
    # these packages are the ones that have the latest chlog_date . 
    # If the package inserted in UDD, has a chlog_date earlier than its duplicate,
    # Delete the record of this package from UDD and insert its duplicate in UDD.
    for d in dup_pkgs:

        dup_query = "SELECT package, chlog_date FROM %s WHERE package='%s'" %(my_config['table'], d['package'])

        cur.execute(dup_query)
        c = cur.fetchone()

        # chlog_date of package inserted in UDD
        udd_date = " ".join(c[1].split()[:-1])
        udd_date = datetime.strptime(udd_date, '%a, %d %b %Y %H:%M:%S')

        # chlog_date for duplicated package not in UDD
        dup_date = " ".join(d['chlog_date'].split()[:-1])
        dup_date = datetime.strptime(dup_date, '%a, %d %b %Y %H:%M:%S')

        # compare the chlog_date of the UDD package and its duplicate
        # if udd_date > dup_date -> do nothing
        # else: delete the udd package and insert its duplicate
        if udd_date < dup_date:
            del_query = "DELETE FROM ONLY %s WHERE package='%s'" %(my_config['table'], d['package'])
            cur.execute(del_query)
            cur.execute(pkgquery, d)

        # test_query = "SELECT package, chlog_date FROM %s WHERE package='%s'" %(my_config['table'], d['package'])
        # cur.execute(test_query)
        # c = cur.fetchone()

    cur.execute("DEALLOCATE package_insert")
    
    # Inserting references should be save because above we are testing for existant table entries
    query = """PREPARE bibref_insert (text, text, text, text, int) AS INSERT INTO bibref
                   (source, key, value, package, rank)
                    VALUES ($1, $2, $3, $4, $5)"""
    cur.execute(query)
    bibquery = "EXECUTE bibref_insert (%(source)s, %(key)s, %(value)s, %(package)s, %(rank)s)"
    try:
      cur.executemany(bibquery, bibrefs)
    except ProgrammingError:
      print "Error while inserting references"
      raise
    except IntegrityError, err:
      print "IntegrityError: %s\n%s" % (str(err), bibrefs)
      raise
    except:
      print "unknown error\n%s" % bibrefs
      raise
    self.connection.commit()
    cur.execute("DEALLOCATE bibref_insert")

    # Inserting upstream metadata should be save because above we are testing for existant table entries
    query = """PREPARE upstream_insert (text, text, text) AS INSERT INTO upstream_metadata
                   (source, key, value)
                    VALUES ($1, $2, $3)"""
    cur.execute(query)
    uquery = "EXECUTE upstream_insert (%(source)s, %(key)s, %(value)s)"
    try:
      cur.executemany(uquery, umetadata)
    except ProgrammingError:
      print "Error while inserting references"
      raise
    cur.execute("DEALLOCATE upstream_insert")

    # Inserting edam data
    if edamdata:
      query = """PREPARE edam_insert (text, text, text[], jsonb) AS INSERT INTO edam
                     (source, package, topics, scopes)
                      VALUES ($1, $2, $3, $4)"""
      cur.execute(query)
      equery = "EXECUTE edam_insert (%(source)s, %(package)s, %(topics)s, %(scopes)s)"
      try:
        cur.executemany(equery, edamdata)
      except ProgrammingError:
        print "Error while inserting references"
        raise
      except KeyError, err:
        print "Wrong key while inserting references (%s)" % (str(err))
        print edamdata
        raise
      cur.execute("DEALLOCATE edam_insert")
    if registrydata:
      query = """PREPARE registry_insert (text, text, text) AS INSERT INTO registry
                     (source, name, entry)
                      VALUES ($1, $2, $3)"""
      cur.execute(query)
      rquery = "EXECUTE registry_insert (%(source)s, %(Name)s, %(Entry)s)"
      try:
        cur.executemany(rquery, registrydata)
      except ProgrammingError:
        print "Error while inserting registry data"
        raise
      except KeyError, err:
        print "Wrong key while inserting registry data (%s)" % (str(err))
        print registrydata
        for r in registrydata:
          if not r.has_key('source'):
            print "this entry has no source value:", str(r)
        raise
      cur.execute("DEALLOCATE registry_insert")

    bibref = bibtex_check(cur, self.log)
    bibref.check()
    
    cur.execute("ANALYZE %s" % my_config['table'])

if __name__ == '__main__':
  main()

# vim:set et tabstop=2:
