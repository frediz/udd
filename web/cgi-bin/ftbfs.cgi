#!/usr/bin/python3
# -*- coding: utf-8; mode: python; tab-width: 4; -*-
#
# Copyright (C) 2017, Erwan Prioul <erwan@linux.vnet.ibm.com>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Display FTBFS packages on given arch
"""

import os
import sys
import cgi
import datetime
import psycopg2

sys.path.insert(0, os.path.abspath('../../pylibs/'))
from cgi_helpers import *

DATABASE = 'service=udd'


class AttrDict(dict):
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            self[key] = value

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError as e:
            raise AttributeError(e)


def query(query, cols, **parameters):
    try:
        conn = psycopg2.connect(DATABASE)
        cursor = conn.cursor()
        cursor.execute(query, parameters)
    except Exception:
        exit(1)
    for row in cursor.fetchall():
        yield AttrDict(**dict(zip(cols, row)))
    cursor.close()
    conn.close()


def pretty_time_delta(when):
    seconds = (datetime.datetime.now() - when).total_seconds()
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return '%dd' % (days)
    elif hours > 0:
        return '%dh' % (hours)
    elif minutes > 0:
        return '%dm' % (minutes)
    else:
        return '%ds' % (seconds)


def packageLine(packages, package, release, pending=False):
    package_escaped = package.replace('+', '%2B')
    bugs = "&nbsp;"
    what = 'nopatch'
    if pending:
        what = 'patch'
    if len(packages[package][what]) > 0:
        bugs = ''.join('<a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=%d">#%d</a> - %s<br>' % (x[0], x[0], x[1]) for x in sorted(packages[package][what]))
    html = (
        '<tr><td class="package">%s_%s</td>' % (package, packages[package]['version']) +
        '<td class="links">' +
        '<a href="https://buildd.debian.org/status/package.php?p=%s&suite=%s" title="Debian buildd status">%s</a> ' % (package_escaped, release, pretty_time_delta(packages[package]['state_change'])) +
        '[<a href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?src=%s" title="Debian bugs in source package">B</a>]' % (package_escaped) +
        '[<a href="http://buildd.debian.org/status/logs.php?pkg=%s" title="Debian build logs">L</a>]' % (package_escaped) +
        '[<a href="https://tracker.debian.org/pkg/%s" title="Debian Package Tracker">T</a>]</td>' % (package_escaped) +
        '<td>%s</td></tr>' % bugs
    )
    return html


def generatePending(packages, nb, arch, release):
    return \
        '<div class="claim" style="margin-top:20px;">%d FTBFS packages on <span class="arch">%s</span> with a patch pending</div><table>' % (nb, arch) + \
        ''.join(packageLine(packages, x, release, True) for x in sorted(packages.keys()) if len(packages[x]['patch']) > 0) + \
        '</table>'


def generateFailing(packages, failing, nb, arch, release):
    html = '<div class="claim">%d FTBFS packages on <span class="arch">%s</span> without patch</div>' % (nb, arch)
    for c in sorted(failing.keys()):
        if c > 2:
            txt = "and %d other architectures" % (c - 1)
        elif c == 2:
            txt = "and 1 other architecture"
        else:
            txt = ""
        html += \
            '<div class="banner">%d packages are failing on <span class="arch">%s</span> %s</div><table>' % (len(failing[c]), arch, txt) + \
            ''.join(packageLine(packages, x, release) for x in sorted(failing[c])) + \
            '</table>'
    return html


QUERY_ARCH_FTBFS_PKGS = ("""
SELECT distinct source, version, state_change
  FROM wannabuild
 WHERE architecture = %(arch)s
       AND distribution = %(dist)s
       AND state IN
           ('Failed', 'Build-Attempted')
""")
def getPackages(arch, release):
    # Get all FTBFS packages on given architecture
    packages = {
        x['s']: {
            'version': x['version'],
            'state_change': x['state_change'],
            'patch': [],
            'nopatch': []
        } for x in query(
            QUERY_ARCH_FTBFS_PKGS, ('s', 'version', 'state_change'), arch=arch, dist=release)
    }
    # Get opened bugs with the given architecture as tag
    QUERY_OPEN_BUGS_IN_ARCH = ("""
SELECT distinct source, id, title
  FROM bugs
       INNER JOIN bugs_usertags
       USING (id)
 WHERE status != 'done'
       AND bugs_usertags.tag = %(tag)s
       AND source = ANY
           (%(packages)s)
""")
    bugs = {x['id']: [x['source'], x['title']] for x in
        query(QUERY_OPEN_BUGS_IN_ARCH, ('source', 'id', 'title'), tag=arch,
        packages=[*packages])}

    if bugs:
        # Which bugs have a patch?
        QUERY_BUGS_WITH_PATCH = ("""
SELECT id
  FROM bugs_tags
 WHERE tag = 'patch'
       AND id = ANY
           (%(bugid)s)
""")
        patched = [x['i'] for x in query(QUERY_BUGS_WITH_PATCH, ('id'), bugid=[*bugs])]
        for b in bugs.keys():
            where = 'nopatch'
            if b in patched:
                where = 'patch'
            packages[bugs[b][0]][where].append([b, bugs[b][1]])

    l = [x for x in packages.keys() if len(packages[x]['patch']) <= 0]
    countFailing = len(l)
    countPending = len(packages.keys()) - countFailing
    # For a FTBFS package, get the number of architectures it also fails on
    QUERY_FTBFS_PKG_ARCHES = ("""
  SELECT source, count(*) AS c
    FROM wannabuild
   WHERE distribution = %(dist)s
         AND state IN
             ('Failed', 'Build-Attempted')
         AND source = ANY
             (%(packages)s)
GROUP BY source
""")
    failing = {}
    for r in query(QUERY_FTBFS_PKG_ARCHES, ('source', 'c'), packages=l, dist=release):
        if r['c'] not in failing:
            failing[r['c']] = []
        failing[r['c']].append(r['source'])
    return """
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>FTBFS packages on %s in %s</title>
<style>
table {
    width: 100%%;
    border-collapse: collapse;
    table-layout: fixed;
}
tr:nth-child(2n+1) {
    background-color: #fbfbfb;
}
td {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis !important;
    vertical-align: top;
    padding-left: 4px;
}
.package {
    width: 375px;
}
.links {
    width: 150px;
    text-align: right;
}
.claim {
    background-color: #dddddd;
    padding: 10px;
    padding-left: 4px !important;
    font-size: 110%%;
}
.banner {
    background-color: #eeeeee;
    padding: 4px;
}
.arch {
    font-style: oblique;
}
</style>
</head>
<body>
""" % (arch, release) + generateFailing(packages, failing, countFailing, arch,
        release) + generatePending(packages, countPending, arch, release) + '</body></html>'


def getArchs(release):
    QUERY_ARCHES = ("""
  SELECT DISTINCT(architecture) AS a
    FROM wannabuild
   WHERE distribution = %(dist)s
         AND state IN ('Failed', 'Build-Attempted')
ORDER BY a
""")
    return [x['a'] for x in query(QUERY_ARCHES, ('a'), dist=release)]


def getFailingReleases():
    QUERY_FAILING_RELEASES = """
    SELECT DISTINCT(distribution) AS d
      FROM wannabuild
     WHERE state IN ('Failed', 'Build-Attempted')
  ORDER BY d
"""
    return [x['r'] for x in query(QUERY_FAILING_RELEASES, ('r'))]


def getForm(archs, releases):
    return """
<!DOCTYPE html>
<html>
<head>
<title>FTBFS packages on given architecture and release</title>
<style>
.claim {
    background-color: #dddddd;
    padding: 10px;
    padding-left: 4px !important;
    font-size: 110%%;
    margin-bottom: 5px;
}
</style>
</head>
<body>
<div class="claim">FTBFS packages</div>
<form action="?" accept-charset="UTF-8">
Architecture: <select name="arch" id="arch"><option value=""></option>%s</select>
Release: <select name="release" id="release"><option value=""></option>%s</select>
<input type="submit" value="Show packages">
</form>
</body>
</html>
""" % (
''.join('<option value="%s">%s</option>' % (x, x) for x in archs),
''.join('<option value="%s">%s</option>' % (x, x) for x in releases)
)


def main():
    print_contenttype_header('text/html')
    form = cgi.FieldStorage()

    releases = getFailingReleases()
    release = form.getfirst('release', 'sid')
    arch = form.getfirst('arch')

    if release not in releases:
        release = 'sid'
    archs = getArchs(release)
    if arch is not None and arch in archs:
        print(getPackages(arch, release))
    else:
        archs = getArchs('sid')
        print(getForm(archs, releases))


if __name__ == '__main__':
    main()
